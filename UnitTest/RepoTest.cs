﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Repositories;
using System.Collections;
using WebApiMAS.Controllers;
using System.Web.Http.Results;
using System.Collections.Generic;
using WebApiMAS.Models;

namespace UnitTest {
	[TestClass]
	public class RepoTest {
		[TestMethod]
		public void Test_GetEmployeesList() {
			var resultList = EmployeesRepo.GetList().GetAwaiter().GetResult();

			Assert.IsInstanceOfType( resultList, typeof( IEnumerable ), "Result is not a valid list." );
		}
		[TestMethod]
		public void Test_GetEmployees_Success() {
			// Set up Prerequisites   
			var controller = new EmployeesController();
			// Act on Test  
			var response = controller.Get( null );
			var contentResult = response as OkNegotiatedContentResult<HttpResultContainer>; 
			// Assert the result  
			Assert.IsNotNull( contentResult );
			Assert.IsNotNull( contentResult.Content );
			Assert.IsNotNull( contentResult.Content.payload );
		}
	}
}
