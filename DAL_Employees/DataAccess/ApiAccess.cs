﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace DAL_Employees {
	public class ApiAccess {

		#region PROPERTIES

		private static Lazy<HttpClient> _client = new Lazy<HttpClient>( InitializeClient );
		/// <summary>
		/// Singleton instance of an HTTP Client
		/// </summary>
		private static HttpClient Client {
			get { return _client.Value; }
		}

		public static String BaseAddress {
			set {
				if ( null == Client.BaseAddress
					|| String.IsNullOrEmpty( ( Client.BaseAddress.OriginalString ) ) )
					Client.BaseAddress = new Uri( value.TrimEnd( '/' ) + '/' );
			}
		}

		#endregion PROPERTIES

		#region METHODS

		#region PUBLIC

		public static async Task<string> GetDataAsString( string urlPath ) {

			String output = null;

			if ( null == Client.BaseAddress )
				throw new Exception( "No Base address provided for the connection." );

			try {
				using ( HttpResponseMessage response = await Client.GetAsync( urlPath ) ) {
					if ( response.IsSuccessStatusCode ) {
						output = await response.Content.ReadAsStringAsync();
					}
				}

			} catch ( Exception err ) {
				output = err.Message;
			}
			return output;
		}

		#endregion PUBLIC

		#region PRIVATE

		private static HttpClient InitializeClient() {
			HttpClient client = new HttpClient();

			client.DefaultRequestHeaders.Accept.Clear();
			client.DefaultRequestHeaders.Add( "User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36" );

			return client;
		}

		#endregion PRIVATE

		#endregion METHODS

	}
}
