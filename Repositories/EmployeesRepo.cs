﻿using BL_Employees;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repositories {
	public static class EmployeesRepo {

		#region PROPERTIES

		private static List<BL_Employees.DTO.Employee> _EmployeesList = null;

		#endregion PROPERTIES

		#region METHODS

		public static async Task<List<BL_Employees.DTO.Employee>> GetList() {
			if ( null == _EmployeesList ) {
				DAL_Employees.ApiAccess.BaseAddress = "http://masglobaltestapi.azurewebsites.net/";
				var response = await DAL_Employees.ApiAccess.GetDataAsString( "/api/Employees" );
				_EmployeesList = EmployeeFactory.ParseList( response );
			}

			return _EmployeesList;
		}

		public static BL_Employees.DTO.Employee GetById( int id ) {
			return GetList().Result.FirstOrDefault( e => e.Id == id );
		}

		#endregion METHODS

	}
}
