﻿using System;

namespace BL_Employees.DTO {

	#region CLASS

	public class Employee : BL_Employees.Contract.Employee {

		public ContractType TypeOfContract;

		public Double AnnualSalary {
			get {

				switch ( TypeOfContract ) {

					case ContractType.Hourly:
						return 120 * HourlySalary * 12;

					case ContractType.Monthly:
						return MonthlySalary * 12;

				}

				return default( double );
			}
		}

	}

	#endregion CLASS

}
