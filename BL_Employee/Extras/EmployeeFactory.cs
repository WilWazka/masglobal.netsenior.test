﻿using BL_Employees.DTO;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;
using System.Text;

namespace BL_Employees {

	#region ENUMERATIONS

	public enum ContractType {
		Hourly,
		Monthly
	}

	#endregion ENUMERATIONS

	#region CLASS

	public static class EmployeeFactory {

		#region PROPERTIES

		private static Lazy<DataContractJsonSerializer> _serializer = new Lazy<DataContractJsonSerializer>( () => {
			return new DataContractJsonSerializer( typeof( IEnumerable<BL_Employees.Contract.Employee> ) );
		} );

		private static DataContractJsonSerializer Serializer {
			get { return _serializer.Value; }
		}

		#endregion PROPERTIES

		#region FACTORY METHODS

		public static List<Employee> ParseList( string jsonString ) {
			var inputList = (BL_Employees.Contract.Employee[])Serializer.ReadObject( new System.IO.MemoryStream( Encoding.UTF8.GetBytes( jsonString ?? "" ) ) );

			return new List<Employee>( Array.ConvertAll( inputList, From ) );
		}

		public static Employee From( BL_Employees.Contract.Employee entity ) {
			var output = new BL_Employees.DTO.Employee {
				Id = entity.Id,
				ContractTypeName = entity.ContractTypeName,
				Name = entity.Name,
				RoleId = entity.RoleId,
				RoleName = entity.RoleName,
				RoleDescription = entity.RoleDescription,
				HourlySalary = entity.HourlySalary,
				MonthlySalary = entity.MonthlySalary
			};

			if ( entity.ContractTypeName.Contains( "Hourly" ) )
				output.TypeOfContract = ContractType.Hourly;

			if ( entity.ContractTypeName.Contains( "Monthly" ) )
				output.TypeOfContract = ContractType.Monthly;

			return output;
		}

		#endregion FACTORY METHODS

	}

	#endregion CLASS
}
