﻿using System;
using System.Runtime.Serialization;

namespace BL_Employees.Contract {
	[DataContract]
	public class Employee {

		[DataMember( Name = "id" )]
		public Int32 Id;

		[DataMember( Name = "name" )]
		public string Name;

		[DataMember( Name = "contractTypeName" )]
		public string ContractTypeName;

		[DataMember( Name = "roleId" )]
		public Int32 RoleId;

		[DataMember( Name = "roleName" )]
		public string RoleName;

		[DataMember( Name = "roleDescription" )]
		public string RoleDescription;

		[DataMember( Name = "hourlySalary" )]
		public Double HourlySalary;

		[DataMember( Name = "monthlySalary" )]
		public Double MonthlySalary;

	}
}
