﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiMAS.Models {
	public class HttpResultContainer {
		public bool success { get; set; }
		public string message { get; set; }
		public Object payload { get; set; }

		public HttpResultContainer( Object payload ) {
			success = true;
			message = "";
			this.payload = payload;
		}

		public HttpResultContainer() : this( null ) { }
	}
}