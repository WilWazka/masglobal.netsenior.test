﻿using Repositories;
using System;
using System.Web.Http;
using System.Web.Http.Description;
using WebApiMAS.Models;

namespace WebApiMAS.Controllers {
	public class EmployeesController : ApiController {

		// GET: api/GetList/5
		[ResponseType( typeof( BL_Employees.DTO.Employee ) )]
		public  IHttpActionResult Get(int? id) {
			var output = new HttpResultContainer();

			try {
				if ( !id.HasValue ) {
					var resultList = EmployeesRepo.GetList().GetAwaiter().GetResult();
					if ( resultList == null ) {
						return NotFound();
					}
					output.payload = resultList;
				} else {
					var resultList = EmployeesRepo.GetById( id.Value );
					if ( resultList == null ) {
						return NotFound();
					}
					output.payload = resultList;
				}

			} catch ( Exception err ) {
				output = new HttpResultContainer( err ) {
					message = err.Message,
					success = false
				};

			}
			return Ok( output );
		}

	}
}